require 'sinatra'
require './lib/player'
require './lib/board'
require './lib/game'

class App < Sinatra::Base 

    def initialize
        super()
    @board = Board.new(4,4)
    @players = nil
    @@game = nil
    @@abandon = false
    end

    enable :sessions

    #Welcome 
    get '/' do
        erb:welcome
    end

    #Initial Configuration
    get '/initialConfig' do
        erb:initialConfiguration
    end
   
    get '/startGame' do
        @numberOfPlayers= params[:numberOfPlayers]
        if (@@abandon == false)
            
        
            @@game = Game.new(@numberOfPlayers.to_i, @board);
    
            @players = @@game.getPlayers
            @numPlayers = @@game.getNumberOfPlayers
            @board = @@game.createBoard
            @currentPlayer = @@game.getCurrentPlayer
        else  
            @board = Board.new(4,4)
            @@game = Game.new(@numberOfPlayers.to_i, @board);
            @players = @@game.getPlayers
            @numPlayers = @@game.getNumberOfPlayers
            @board = @@game.resetBoard
            @currentPlayer = @@game.getCurrentPlayer
            end



        
        erb:gameView
    end

    post '/move' do
        
        @positionRow= params[:positionRow].to_i
        @positionColumn= params[:positionColumn].to_i
        lineLocation= params[:lineLocation]
        @@game.move(@positionRow.to_i, @positionColumn.to_i, lineLocation )
        @players = @@game.getPlayers
        @numPlayers = @@game.getNumberOfPlayers
        @board = @@game.createBoard
        @currentPlayer = @@game.getCurrentPlayer
       
        if @@game.gameEnded == false
            erb:gameView
          else
            @winner = @@game.winner
            erb:gameResultView
        end
        
    end

    get '/restartGame' do
        @board = Board.new(4,4)
        @numPlayers = @@game.getNumberOfPlayers
        @@game = Game.new(@numPlayers, @board);

        
        @players = @@game.getPlayers
        @board = @@game.resetBoard
        @currentPlayer = @@game.getCurrentPlayer
        erb:gameView
    end

    get '/abandonGame' do
        
        @@abandon = true
        erb:initialConfiguration
    end

    get '/showRules' do
        erb:rules
    end

    run! if app_file == $0


end