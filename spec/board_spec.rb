require "./lib/board"
require "./lib/box"

describe Board do 

      before(:each)do
            @board = Board.new(5,5)
      end
      
      it "Crear tablero con 5 filas" do
            expect(@board.getRows).to eq 5
      end

      it "Crear tablero con 5 filas y 5 columnas" do
            expect(@board.getRows).to eq 5
            expect(@board.getColumns).to eq 5
      end

      it "Deberia crear una matriz" do
            expect(@board.getMatrix).not_to eq nil
      end

      it "Deberia obtener una casilla" do
            expect(@board.getBox(0,0)).not_to eq nil
      end

      it "Deberia marcar lado de arriba de la casilla 0,0" do
            @board.markUpSide(0,0)
            box = @board.getBox(0,0)
            expect(box.getUp).to eq true
      end

      it "Deberia marcar lado de abajo de la casilla 0,0" do
            @board.markDownSide(0,0)
            box = @board.getBox(0,0)
            expect(box.getDown).to eq true
      end

      it "Deberia marcar lado derecho de la casilla 0,0" do
            @board.markRightSide(0,0)
            box = @board.getBox(0,0)
            expect(box.getRight).to eq true
      end

      it "Deberia marcar lado de arriba de la casilla 0,0" do
            @board.markLeftSide(0,0)
            box = @board.getBox(0,0)
            expect(box.getLeft).to eq true
      end

      it "Deberia marcar el lado izquierdo de casilla 0,1 al marcar el lado izquierdo de casilla 0,0" do
            @board.markRightSide(0,0)
            box = @board.getBox(0,1)
            expect(box.getLeft).to eq true
      end

      it "Deberia marcar el lado derecho de casilla 0,0 al marcar el lado izquierdo de casilla 0,1" do
            @board.markLeftSide(0,1)
            box = @board.getBox(0,0)
            expect(box.getRight).to eq true
      end

      it "Deberia marcar el lado de arriba de casilla 1,0 al marcar el lado abajo de casilla 0,0" do
            @board.markDownSide(0,0)
            box = @board.getBox(1,0)
            expect(box.getUp).to eq true
      end

      it "Deberia marcar el lado de abajo de casilla 0,0 al marcar el lado arriba de casilla 1,0" do
            @board.markUpSide(1,0)
            box = @board.getBox(0,0)
            expect(box.getDown).to eq true
      end

end
