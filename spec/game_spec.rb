require './lib/player'
require './lib/board'
require './lib/game'


describe Game do
    
    before(:each) do
        tablero = Board.new(4,4)
        @game = Game.new(3,tablero)
        @board = @game.getBoard
    end

    it 'deberia crear tablero de 4 filas' do
        expect(@game.getBoardRowsSize).to eq 4
    end

    it 'deberia crear tablero de 4 columnas' do
        expect(@game.getBoardColumnSize).to eq 4
    end

    it 'deberia crear 3 jugadores' do
        expect(@game.getNumberOfPlayers).to eq 3
    end

    it 'deberia marcar lado de arriba de la casilla 1,0 y 
    en la casilla 0,0 el lado de abajo' do
        @game.move(1,0,'up')

        box1 = @board.getBox(1,0)
        box2 = @board.getBox(0,0) 
        expect(box1.getUp).to eq true
        expect(box2.getDown).to eq true
    end

    it 'deberia marcar lado de abajo de la casilla 0,0 y 
    en la casilla 1,0 el lado arriba' do
        @game.move(0,0,'down')
        
        box1 = @board.getBox(0,0)
        box2 = @board.getBox(1,0) 
        expect(box1.getDown).to eq true
        expect(box2.getUp).to eq true
    end

    it 'deberia marcar lado de derecha de la casilla 0,0 y 
    en la casilla 0,1 el lado izquierdo' do
        @game.move(0,0,'right')
        
        box1 = @board.getBox(0,0)
        box2 = @board.getBox(0,1) 
        expect(box1.getRight).to eq true
        expect(box2.getLeft).to eq true
    end


    it 'deberia marcar lado de izquierdo de la casilla 0,1 y 
    en la casilla 0,0 el lado derecho' do
        @game.move(0,1,'left')
        
        box1 = @board.getBox(0,1)
        box2 = @board.getBox(0,0) 
        expect(box1.getLeft).to eq true
        expect(box2.getRight).to eq true
    end

    it 'deberia crear un tablero de tamanio 4' do
        expect(@game.createBoard).to eq "<tr>   <td class=''>0 - 0</td>   <td class=''>0 - 1</td>   <td class=''>0 - 2</td>   <td class=''>0 - 3</td></tr><tr>   <td class=''>1 - 0</td>   <td class=''>1 - 1</td>   <td class=''>1 - 2</td>   <td class=''>1 - 3</td></tr><tr>   <td class=''>2 - 0</td>   <td class=''>2 - 1</td>   <td class=''>2 - 2</td>   <td class=''>2 - 3</td></tr><tr>   <td class=''>3 - 0</td>   <td class=''>3 - 1</td>   <td class=''>3 - 2</td>   <td class=''>3 - 3</td></tr>"
    end

    it 'deberia marcar lado de arriba de celda elegida en la tabla html' do
        tablero = Board.new(1,1)
        game = Game.new(2,tablero)
        board = @game.getBoard
        game.move(0, 0, "up" )
        expect(game.createBoard).to eq "<tr>   <td class=' topSelected'>0 - 0</td></tr>"
    end

    it 'deberia marcar lado de abajo de celda elegida en la tabla html' do
        tablero = Board.new(1,1)
        game = Game.new(2,tablero)
        board = @game.getBoard
        game.move(0, 0, "down" )
        expect(game.createBoard).to eq "<tr>   <td class=' downSelected'>0 - 0</td></tr>"
    end

    it 'deberia marcar lado de izquierda de celda elegida en la tabla html' do
        tablero = Board.new(1,1)
        game = Game.new(2,tablero)
        board = @game.getBoard
        game.move(0, 0, "left" )
        expect(game.createBoard).to eq "<tr>   <td class=' leftSelected'>0 - 0</td></tr>"
    end

    it 'deberia marcar lado de derecha de celda elegida en la tabla html' do
        tablero = Board.new(1,1)
        game = Game.new(2,tablero)
        board = @game.getBoard
        game.move(0, 0, "right" )
        expect(game.createBoard).to eq "<tr>   <td class=' rightSelected'>0 - 0</td></tr>"
    end

    it 'Deberia verificar al marcar el ultimo lado de una casilla gana un punto' do
        @game.move(0, 0, "right")
        @game.move(0, 0, "left")
        @game.move(0, 0, "up")
        @game.move(0, 0, "down")
        expect(@game.verifyPoint(0,0)).to eq true
    end

    it 'Deberia empezar el turno actual en el primer jugador' do
        expect(@game.getTurn).to eq 0
    end

    it 'Deberia cambiar al siguiente jugador' do
        @game.move(0, 0, "right")
        expect(@game.getTurn).to eq 1
    end

    it 'Deberia volver el turno al jugador inicial' do
        @game.move(0, 0, "right")
        @game.move(0, 0, "up")
        @game.move(0, 0, "left")
        expect(@game.getTurn).to eq 0
    end

    it 'Deberia devolver el jugador actual' do
        @game.move(0, 0, "right")
        expect(@game.getTurn).to eq 1
    end

    it 'Deberia devolver un jugador valido' do
        expect(@game.getCurrentPlayer).not_to eq nil
    end

    it 'Deberia aumentar un punto en el score del jugador actual al marcar el ultimo lado de una celda' do
        @game.move(0, 0, "right")
        @game.move(0, 0, "left")
        @game.move(0, 0, "up")
        @game.move(0, 0, "down")
        expect(@game.getCurrentPlayer.getScore).to eq 1
    end

    it 'Deberia incrementar el score del segundo jugador' do
        @game.move(1, 1, "down")
        @game.move(0, 0, "right")
        @game.move(0, 0, "left")
        @game.move(0, 0, "up")
        @game.move(1, 1, "up")
        @game.move(0, 0, "down")
        expect(@game.getCurrentPlayer.getScore).to eq 1
    end

    it 'Deberia devolver el nombre que se asigno al primer jugador' do
        player1 = @game.getPlayers[0]
        player1.setName("Jugador 1")
        expect(player1.getName).to eq "Jugador 1"
    end

    it 'Deberia devolver el nombre que se asigno al segundo jugador' do
        player2 = @game.getPlayers[1]
        player2.setName("Jugador 2")
        expect(player2.getName).to eq "Jugador 2"
    end
    
    it 'Deberia mantenerse el turno actual si el jugador marco un punto' do
        @game.move(2,2, "up")
        @game.move(2,2, "down")
        @game.move(2,2, "right")
        @game.move(2,2, "left")
        expect(@game.getTurn). to eq 0
    end

    it 'Deberia crear 3 jugadores' do
        expect(@game.getNumberOfPlayers).to eq 3
    end

    it 'Deberia verificar que el juego aun no termino' do 
        expect(@game.gameEnded).to eq false
    end

    it 'Deberia verificar que el juego termino' do
        @game.completeBoard
        expect(@game.gameEnded).to eq true
    end

    it 'Deberia devolver el nombre del jugador con el score mas alto' do
        score = 5
        for i in 0..(@game.getNumberOfPlayers - 1) do
            @game.getPlayers[i].setScore(score)
            score = score - 1
        end
        expect(@game.winner).to eq 'Jugador 1'
    end

    it 'Deberia mantener el fullbox falso si solo se marcan 2 lados de una celda' do
        @game.move(2,2, "up")
        @game.move(2,2, "down")
        @game.move(2,2, "left")
        @game.move(2,2, "right")
        @game.move(2,1, "up")
        expect(@game.verifyPoint(2,1)).to eq false
    end

    it 'Deberia resetear el tablero' do

        expect(@game.resetBoard).to eq "<tr>   <td>0 - 0</td>   <td>0 - 1</td>   <td>0 - 2</td>   <td>0 - 3</td></tr><tr>   <td>1 - 0</td>   <td>1 - 1</td>   <td>1 - 2</td>   <td>1 - 3</td></tr><tr>   <td>2 - 0</td>   <td>2 - 1</td>   <td>2 - 2</td>   <td>2 - 3</td></tr><tr>   <td>3 - 0</td>   <td>3 - 1</td>   <td>3 - 2</td>   <td>3 - 3</td></tr>"
    end
    
end