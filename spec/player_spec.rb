require './lib/player'

describe Player do

      before(:each) do
            @player = Player.new()
      end

      it "Crear jugador con nombre 'Jugador 1'" do
            @player.setName("Jugador 1")
            expect(@player.getName).to eq "Jugador 1"
      end

      it "Crear jugador con score en 0" do
            expect(@player.getScore).to eq 0
      end

      it "Deberia incrementar el score a 1" do
            @player.winPoint()
            expect(@player.getScore).to eq 1
      end

end
