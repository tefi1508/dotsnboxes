require './lib/box'

describe Box do

      before(:each) do
            @box = Box.new()
      end

      it "Crear box con lado superior" do
            expect(@box.getUp).to eq false
      end

      it "Crear box con lado abajo" do
            expect(@box.getDown).to eq false
      end

      it "Crear box con lado derecho" do
            expect(@box.getRight).to eq false
      end

      it "Crear box con lado izquierdo" do
            expect(@box.getLeft).to eq false
      end

      it "Deberia seleccionar un lado de la casilla" do
            @box.markSide("up")
            expect(@box.getUp()).to eq true 
      end

      it "Deberia devolver 'true' si todas las casillas estan marcadas" do
            @box.markSide("up")
            @box.markSide("down")
            @box.markSide("right")
            @box.markSide("left")
            expect(@box.fullBox).to eq true
      end

end
