Feature: Game
      Como jugador
      Quiero ver el juego 
      Para jugar 

      Scenario: Ver aniadir linea
            Given visito la pagina del juego
            Then deberia ver subtitulo "Añadir Linea"

      Scenario: Ver las entradas para marcar una celda
            Given visito la pagina del juego
            Then deberia ver posicion fila 
            Then deberia ver posicion columna
            Then deberia ver opciones de lugar de celda

      Scenario: Marcar lado de una celda
            Given visito la pagina del juego
            Then se visualiza el boton "Agregar"

      Scenario: Ver mensaje de Puntaje
            Given visito la pagina del juego
            Then deberia ver texto de "Puntaje"

      Scenario: Ver seccion de puntaje
            Given visito la pagina del juego
            Then deberia ver la seccion donde se encuntra el puntaje de los jugadores

      Scenario: Ver informacion de los jugadores
            Given visito la pagina del juego
            Then deberia existir seccion con informacion de los jugadores

      