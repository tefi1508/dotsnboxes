Given("estoy en el juego") do
      visit('/startGame')
end
    
When("presionoo el boton {string}") do |boton|
      click_button(boton);
end

Then("deberia empezar un juego nuevo") do
      expect(page).to have_content("Partida");
      visit('/restartGame')
end