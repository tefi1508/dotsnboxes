Given("visito la pagina de configuracion inicial") do
    visit('/initialConfig')
  end
  
  When("presiona el boton {string}") do |string|
    click_button(string)
  end
  
  Then("deberia ver la pagina del juego") do
    expect(page).to have_content("Partida");
  end
  