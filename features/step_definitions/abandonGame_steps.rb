Given("me encuentro en el juego") do
    visit('/startGame')
  end
  
  When("presione el boton {string}") do |string|
    click_button(string)
  end
  
  Then("deberia mostrar mensaje {string}") do |string|
    expect(page).to have_content("Configuracion inicial")
  end