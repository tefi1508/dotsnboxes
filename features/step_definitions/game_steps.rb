Given("visito la pagina del juego") do
    visit('/startGame')
  end
  
  Then("deberia ver subtitulo {string}") do |string|
    expect(page).to have_content("Añadir Linea");
  end
  
  Then("deberia ver posicion fila") do
    expect(page).to have_selector("input[name='positionRow']")
  end
  
  Then("deberia ver posicion columna") do
    expect(page).to have_selector("input[name='positionColumn']")
  end
  
  Then("deberia ver opciones de lugar de celda") do
    expect(page).to have_selector("select[name='lineLocation']")
  end
  
  Then("se visualiza el boton {string}") do |string|
    expect(page).to have_selector("input[value='Agregar']")
  end

  Then("deberia ver texto de {string}") do |string|
    expect(page).to have_content("Puntaje");
  end

  Then("deberia ver la seccion donde se encuntra el puntaje de los jugadores") do
    expect(page).to have_selector('.right-align')
  end
 
  Then("deberia existir seccion con informacion de los jugadores") do
    expect(page).to have_selector('.playersInfo')
  end