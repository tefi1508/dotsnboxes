Feature: Welcome
    Como jugador
    Quiero ver un mensaje de bienvenida 
    Para sentirme mas a gusto

    Scenario: Mensaje de Bienvenida
        Given visito la pagina principal
        Then deberia ver el mensaje "Bienvenido a Dots & Boxes"

    Scenario: Boton iniciar juego
        Given visito la pagina principal
        When presiono el boton "Iniciar Juego"
        Then deberia ver la pagina con titulo "Configuracion inicial"
              