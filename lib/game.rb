require './lib/player'
require './lib/board'

class Game
    

    def initialize(num, board)
        @players = createPlayers(num)
        @board = board;
        @matrix = @board.getMatrix
        @currentTurn = 0
    end

    def createPlayers(num)
        @playersE = Array.new(num)
        
        for i in 0..(num - 1) do
            @playersE[i] =  Player.new()
            @playersE[i].setName("Jugador #{i+1}");
        end
        @playersE
    end

    def getBoardRowsSize
        @board.getRows
    end

    def getBoardColumnSize
        @board.getColumns
    end

    def getNumberOfPlayers
       @players.length 
    end

    def getBoard
        @board
    end

    def completeBoard
        @board.markAllBoxes
    end

    def markCellSide(column, style)
        if column.getUp
            style =  style+" topSelected"
        end
        if column.getDown
            style =  style + " downSelected"
        end
        if column.getRight
            style = style+ " rightSelected"
        end
        if column.getLeft
            style = style +" leftSelected"
        end

        return style
    end

    def createBoard
        table = ""
        i = 0 
        j = 0 
        @matrix.each do |row|
            tr = "<tr>"
            table = table +tr
            row.each do |column|
                style = ""
                td = "   <td class="+style +"></td>"

                #style = markCellSide(column, style)
                if column.getUp
                    style =  style+" topSelected"
                end
                if column.getDown
                    style =  style + " downSelected"
                end
                if column.getRight
                    style = style+ " rightSelected"
                end
                if column.getLeft
                    style = style +" leftSelected"
                end

                style = "'"+style+"'"
                td = "   <td class="+style +">#{i} - #{j}</td>"
               
               table = table + td
               
               j = j + 1
            end
            trEnd = "</tr>"
            table = table + trEnd
            j = 0
            i = i + 1 
        end

        return table
        
    end


    def resetBoard
        table = ""
        i = 0 
        j = 0 

        @matrix.each do |row|
            tr = "<tr>"
            table = table +tr
            row.each do |column|
                td = "   <td>#{i} - #{j}</td>"
               
               table = table + td

               j = j + 1
            end
            trEnd = "</tr>"
            table = table + trEnd

            j = 0
            i = i + 1
        end

        return table
        
    end

    def move (row,column,side)  
        case (side)
        when "up"
            @board.markUpSide(row,column)
        when "down"
            @board.markDownSide(row,column)
        when "right"
            @board.markRightSide(row,column)
        when "left"
            @board.markLeftSide(row,column)
        end

        if !verifyPoint(row,column)
            changeTurn()
        else
            @players[getTurn].winPoint
        end
    end

    def verifyPoint(row,column)
        box = @board.getBox(row,column)
        box.fullBox
    end

    def getTurn
        @currentTurn
    end

    def changeTurn
        if (@currentTurn < (@players.length-1))
            @currentTurn = @currentTurn + 1
        else 
            if (@currentTurn == (@players.length-1))
            @currentTurn = 0
            end
        end
    end
    
    def getCurrentPlayer
        @players[getTurn]
    end

    def getPlayers
        @players
    end

    def gameEnded
        res = true
        for i in 0..(getBoardRowsSize - 1) do
            for j in 0..(getBoardColumnSize - 1) do
                if verifyPoint(i,j) == false
                    res = false
                end
            end 
        end        
       res 
    end

    def winner
        higherScore = 0
        winner = ''
        for i in 0..(getNumberOfPlayers - 1) do
            if @players[i].getScore > higherScore
                winner = @players[i].getName
                higherScore = @players[i].getScore
            end
        end
        winner
    end

end