class Box 

      def initialize
            @up = false
            @down = false
            @righ = false
            @left = false
      end

      def getUp
            @up
      end

      def getDown
            @down
      end

      def getRight
            @righ
      end

      def getLeft
            @left
      end

      def markSide(side)
            case side
            when "up"
                  @up = true
            when "down"
                  @down = true
            when "right"
                  @righ = true
            when "left"
                  @left = true
            end
      end

      def fullBox
            (@up && @down && @righ && @left)
      end

      def clearBox
            @up = false
            @down = false
            @righ = false
            @left = false
      end
end
