require './lib/box'

class Board

      attr_accessor :rows, :columns,:matrix

      def initialize(rows,columns)
            @rows = rows
            @columns = columns
            @matrix=Array.new(@rows) {Array.new(@columns) {Box.new()}}
      end 

      def getRows
            @rows
      end

      def getColumns
            @columns
      end

      def getBox(row,column)
            @matrix[row][column]
      end

      def getMatrix
            @matrix
      end

      def markUpSide(row,column)
            @matrix[row][column].markSide("up")
            if @matrix[row][column].getUp()
                  if row > 0 
                      @matrix[row-1][column].markSide("down")
                  end
            end
      end

      def markDownSide(row,column)
            @matrix[row][column].markSide("down")
            if @matrix[row][column].getDown()
                  if row < @rows-1 
                      @matrix[row+1][column].markSide("up")
                  end
            end
      end

      def markRightSide(row,column)
            @matrix[row][column].markSide("right")
            if @matrix[row][column].getRight()
                  if column < @columns-1 
                      @matrix[row][column+1].markSide("left")
                  end
            end
      end

      def markLeftSide(row,column)
            @matrix[row][column].markSide("left")
            if @matrix[row][column].getLeft()
                  if column > 0
                      @matrix[row][column-1].markSide("right")
                  end
            end
      end

      def markAllBoxes
            for i in 0..(@rows - 1) do
                  for j in 0..(@columns - 1) do
                        @matrix[i][j].markSide("up")
                        @matrix[i][j].markSide("down")
                        @matrix[i][j].markSide("right")
                        @matrix[i][j].markSide("left")
                  end 
            end
      end

      def clear
            for i in 0..(@rows - 1) do
                  for j in 0..(@columns - 1) do
                        @matrix[i][j].clearBox
                  end 
            end
      end

end