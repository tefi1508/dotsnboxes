class Player
      attr_accessor :name, :score

      def initialize()
            @score = 0
      end

      def getName
            @name
      end

      def setName(name)
            @name=name
      end

      def setScore(score)
            @score = score
      end

      def getScore
            @score
      end

      def winPoint
            @score = @score + 1 
      end

end